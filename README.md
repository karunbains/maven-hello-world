#maven-hello-world

Author: Karundeep Bains

Email: 

### Building a release

The following command creates a release, deploys the artefact to the snapshot and release repository and increments the pom for the next release.

```
mvn clean install release:prepare release:perform deploy -P release -s conf/settings.xml -B
```
